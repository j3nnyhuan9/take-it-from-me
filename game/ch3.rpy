label ch3:
    scene black
    with Dissolve(1)

    window show
    n "--CHAPTER 3--"
    window hide
    nvl clear

    scene bg mer room day window open
    with Dissolve(10)
    play music "Sunny Day-SoundBible.com-2064222612.mp3" fadeout 1

    show mer aww at right

    m "It's already morning...?"

    show mer sigh at right
    m "..."

    show mer aww at left with MoveTransition(2)

    m "..."

    scene black
    with Dissolve(.5)
    scene bg stores day
    with Dissolve(1)
    show ar smirk at left
    show mer smirk at right
    m "...Well... I'm glad I finally know your name, Arthur."
    a "Same here, Merlyn."
    a "Hey, I'll catch you around."

    scene black
    with Dissolve(.5)
    scene bg mer room day window open
    with Dissolve(.5)
    
    show mer aww at left

    m "Ugh, I musn't think about him right now."
    m "What time is it?"

    show mer aww at center with MoveTransition(1)
    
    m "..."
    m "Eight, huh?"
    m "I still have a few hours before class starts."
    m "But... there isn't really a point in staying home."

    show mer hmm at center

    m "If I leave now, I can go take a walk in the park again."
    m "Or maybe get some breakfast."
    m "I have... ten dollars. Enough for a decent meal."

    show mer fierce at center 

    m "It should be all right now that it's daytime."

    scene black
    with Dissolve(.5)
    scene bg mer old house foyer day

    show mer fierce at right
    with Dissolve(.5)

    m "Seems like Mom's asleep... that's good."
    
    show mer aww at right

    m "I'm not in the state of mind to deal with anymore nagging."

    hide mer aww at right with Dissolve(.5)
    play sound "Close Door-SoundBible.com-1027341191.mp3"

    scene black 
    with Dissolve(.5)
    scene bg old house 
    with Dissolve(.5)

    m "All right..."
    m "What should I do?"

    menu:
        "Head to the park for some introspection.":
            jump park

        "Get some breakfast at the local cafe.":
            $eat = True
            jump cafe


