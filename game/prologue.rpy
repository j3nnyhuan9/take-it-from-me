label prologue:
    
    window hide
    scene black

    window show
    n "I've always thought I was special."

    n "More special than any other person."

    play music "Wind-Mark_DiAngelo-1940285615.mp3" fadeout 1 loop

    scene bg park2 night
    with Dissolve(.5)
    pause(2)

    n "After all, I was a Magician."

    window hide
    nvl clear
    scene bg playground night
    with Dissolve(.5)

    show mer fierce at left
    m "The air is good."

    play sound "Footsteps-SoundBible.com-534261997.mp3"
    show mer sigh at right with MoveTransition(1)

    stop sound
    m "..."

    show mer worry at right
    m "It sure is lonely here though."

    show mer smirk at right
    m "But I needn't worry."
    m "I am no longer like I was before."

    show mer smirk welcome
    m "Now I am a Magician who can summon her own friends at her own will!"

    play sound "Footsteps-SoundBible.com-534261997.mp3"
    show mer worry at left with MoveTransition(2)

    stop sound
    m "Hmm... but the only question is..."

    show mer fierce at left
    m "Who should I summon?"

    menu:
        "My little sister, Holly.":
            $summon_holly = True
            jump presummon

        "My little brother, Gillard.":
            $summon_holly = False
            jump presummon

    label presummon:
        show mer smirk at left

        if summon_holly:
            m "Hm, I haven't seen Holly in a while."     
            m "I think it a good choice to summon her."   

            scene bg park2 night
            with Dissolve(1)

            m "Now, now, Holly. Come out now and greet your elder sister!"

            play music "Yusuke_Tsutsumi_-_05_-_The_Beach.mp3" fadeout 1

            scene black
            with Dissolve(1)

            scene bg park2 night
            with Dissolve(1)

            show hol ouch at center
            with Dissolve(1)

            pause(.75)

            scene bg playground night
            with Dissolve(1)

            show mer smirk at left
            show hol ouch at right
            with Dissolve(.5)

            h "Ouch... what a ride..."

            show hol surprise at right
            h "Sis! Why did you summon me?"

            show mer worry at left
            m "Because I miss having you around."

            show mer smirk at left
            m "How it goes at your new home?"

            show hol determined at right
            h "Heh, I'm doing quite fine."

            show hol happy at right
            h "But why did you call me?"

            show mer sigh at left
            m "Well, it's lonely. Sit with me, won't you?"

            show hol listening at right 
            h "Oh... oh all right. I've time."

            scene black
            with Dissolve(1)

            jump ch1

        else:
            m "Hm, I haven't seen Gillard in a while."     
            m "I think it a good choice to summon him."   

            scene bg park2 night
            with Dissolve(1)

            m "Now, now, Gillard. Come out now and greet your elder sister!"

            play music "Yusuke_Tsutsumi_-_05_-_The_Beach.mp3" fadeout 1

            scene black
            with Dissolve(1)

            scene bg park2 night
            with Dissolve(1)

            show gil worried at center
            with Dissolve(1)

            pause(.75)

            scene bg playground night
            with Dissolve(1)

            show mer smirk at left
            show gil worried at right
            with Dissolve(.5)

            show gil angry at right
            g "Who in their right mind wanted to summon me...?"
            g "Oh, Sis! It was you?"

            show mer worry at left
            m "That it was. I miss having you around. How it goes at your new home?"

            show gil uhh at right
            g "You needn't ask. I'm doing wonderful without you."

            show gil i see at right
            g "But why did you call me?"

            show mer sigh at left
            m "Well, it's lonely. Sit with me, won't you?"

            show gil uhh at right
            g "Oh... oh all right. I've time."

            scene black
            with Dissolve(1)

            jump ch1

