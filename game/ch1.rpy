label ch1:
    window hide
    scene black
    window show

    n "--CHAPTER 1--"

    nvl clear

    n "When did I realize my talent?"

    n "It isn't easy to pinpoint when."

    scene bg old house gray
    with Dissolve(.5)

    n "I lived in a good household. My mom did her job as a parent."
    n "My siblings and I were typical--once in a while, we'd fight but we all looked out for each other. Save for school, which is often stressful, I was content with life."
    n "So what, you may wonder..."

    scene black
    with Dissolve(.5)

    n "What caused me to have such a talent?"

    nvl clear

    n "I have a hunch. It was when I met my first love: Arthur Montgomery."

    nvl clear
    window hide

    scene bg arch day
    with Dissolve(.5)

    play sound "Shopping Mall Ambiance-SoundBible.com-1942498626.mp3" loop

    pause(1)

    scene bg crossing street day
    with Dissolve(.5)

    show mer worry at left
    show hol listening at center
    show gil huh at right
    with Dissolve(1)

    h "When is Mom coming back?"

    show gil uhh at right
    g "When she comes back."

    show mer aww at left
    m "Have a little patience, will you not, Holly?"

    show mer worry at left
    show hol omg at center
    h "I am patient!"

    show gil i see at right
    g "Says the only person whining about how she can't wait for Mom."

    show hol no way at center
    h "Ugh! You two always gang up on me!"

    show hol omg at right

    h "You know what, I'm not wasting anymore time!"
    show hol omg at offscreenright with move
    hide hol omg
    pause(.50)

    show mer worry at left
    show gil angry at right
    g "H-Hey! Holly, where are you going?!"

    show gil angry at offscreenright with move
    hide gil angry
    
    show mer irritated at right with move
    m "There they go..."

    show mer sigh at left with MoveTransition(1)

    m "I wonder when Gillard will realize that chasing after Holly is a meaningless action."

    show mer irritated at left
    m "She's a wild child and..."

    play sound "Footsteps-SoundBible.com-534261997.mp3"
    show mer curious at center with MoveTransition(2)
    stop sound

    pause(.50)
    
    m "Wait... that kid with the red hair..."
    m "I think I know him..."
    
    play sound "Footsteps-SoundBible.com-534261997.mp3"
    show mer hmm at left with MoveTransition(2)
    stop sound
    m "..."

    show mer worry at left
    m "...Well, since none of my siblings are here waiting for Mom, why should I?"
    show mer aww at left
    m "She can easily call one of us anyway. I think I shall go satisfy my curiosity..."

    show mer worry at offscreenleft with move
    hide mer worry

    scene bg stores day
    show mer curious at left
    with Dissolve(1)

    play music "Kai_Engel_-_06_-_The_Moments_of_Our_Mornings.mp3" fadeout 1 loop
    m "..."
    m "(...What are you doing, Merlyn? Following some random boy...)"
    show mer hmm at left
    m "(Mom would laugh at you...)"
    m "(Let's just leave, Merlyn--)"
    pause(.25)
    show mer curious at left
    m "!"

    scene bg stores day
    show ar normal at right
    with Dissolve(1)
    pause(1)
    
    scene bg stores day
    show mer curious at left
    with Dissolve(1)

    m "(...He's awfully dashing...)"
    
    show mer shock at left
    m "!!"

    show mer worry at left
    m "(Wait, what am I thinking?)"
    show mer sigh at left
    m "(I should go. Gillard must have caught Holly by now. And Mom--)"
    
    pause(.25)

    show ar normal at right
    with Dissolve(.5)

    show mer shock at left
    m "(Oh... oh... he...)"
    m "(He must have noticed... me... staring...)"
    show mer worry at left
    m "(Must... pretend... I wasn't... being a creep...)"
    show mer aww at left
    m "(I know! I should just start walking...!)"

    show mer sigh at center
    show ar normal at center 
    with MoveTransition(.75)
    with vpunch
    
    show mer shock at left
    show ar mmm at right

    m "OH MY GOSH!"
    show mer aww at left
    show ar hmm at right
    m "I'm..."
    show mer aww at left with vpunch
    m " I'm so sorry!"

    "???" "...Uh..."
    show ar smirk at right
    "???" "It's fine. Your tiny frame didn't even brush me."

    show mer worry at left
    m "Ah, er..."
    show mer worry at left with hpunch
    m "Of course! I... er..."

    window show
    n "At that time, I could only feel my throat. I didn't even know I had hands and arms and feet and legs."
    n "And my heart... I can't remember exactly if it was pounding really hard or hardly really pounding."
    n "Yes... all I remember is my throat. It was swallowing air instead of saliva."
    n "And time... time seemed to be so... {cps=10}slow...{/cps}"
    n "I had never felt this before. And to be quite honest... I don't know whether I liked this feeling or not. It was so alien... so... scary, I suppose."
    window hide
    nvl clear


    show ar normal at right
    "???" "Well, if you'll excuse me, I've got to be on my way."
    m "Ah, of course..."

    show ar normal at left
    show mer worry at center 
    with MoveTransition(1.5)

    m "Ah... um..."
    "???" "?"
    "???" "Yes?"
    m "Um..."

    show mer curious at right with MoveTransition(1)
    m "Um..."
    show mer hmm at right
    m "That is..."
    show mer blush hand at right
    m "I was wondering... what is your name?"

    show ar hmm at left
    "???" "..."
    "???" "...Why do you ask? Do I know you?"

    window show
    show mer worry at right
    n "Later on, it turned out I did know him. But at the time, I couldn't remember. I bet if he had asked me for my name, I wouldn't have even remembered mine. I was in such a trance."
    n "But at the same time, I refused to act like a fool before him."

    show mer sigh at right
    n "I sighed and then I breathed in deeply to regain my composure."
    window hide
    nvl clear


    show mer curious at right
    m "Y-You don't remember me from high school?"

    show ar disbelief at left
    "???" "...High school?"
    "???" "Are you sure we met in high school? I think I'd remember someone with such long hair."

    show mer smirk at right
    m "Ahhh, I grew out my hair. And I th-think we've only passed by in the hallways. Didn't... actually talk to each other."
    show mer worry at right
    m "(Do you hear yourself, Merlyn? Speaking such gramatically incorrect sentences! You have to get away from this boy soon!)"
    show mer blush hand at right
    m "Y-You know what, never mind. Maybe I mixed you up with someone."
    m "Sorry to bother you."

    show ar normal at left
    show mer worry at offscreenright with move
    hide mer worry
    
    pause(.25)

    "???" "Hey... wait."

    show mer worry at right with Dissolve(.5)

    "???" "I think you're right."
    
    show ar smirk at left
    "???" "Sorry, I can't remember your name though."

    show mer blush hand at right
    m "Aaaah, th-that's fine."
    m "I'm Merlyn. Like the wizard that counseled King Arthur."

    show ar disbelief at left
    "???" "No way."

    show mer curious at right
    m "Pardon?"

    show ar smirk at left
    "???" "My name's Arthur. Arthur Montgomery."
    
    window show
    n "At that instant, I remembered."
    n "Arthur Montgomery. The newspaper boy. His route passed by my house every day, save for the weekends. He and his shiny metallic gray bike never lingered long enough for me to catch a glimpse of his face."
    n "But his red hair, his signature jacket, and the fact that I passed by him at school was enough to make me more than interested in him."
    n "And now that I got to see his face... I was glad that I was interested in him. He was... cool."
    window hide
    nvl clear
    
    show mer smirk at right
    m "...Well... I'm glad I finally know your name, Arthur."
    a "Same here, Merlyn."
    a "Hey, I'll catch you around."

    show ar smirk at offscreenleft with MoveTransition(1)
    hide ar smirk

    show mer curious at center with MoveTransition(1)
    m "..."
    show mer sigh at center
    m "...Should get back. I'd hate for Mom to send a search party for me."

    show mer worry at offscreenleft with move 
    hide mer worry

    scene bg crossing street day
    with Dissolve(.5)

    show mer worry at left
    m "Hmm, they haven't returned...?"

    show mer fierce at left
    show gil i see at right
    show hol sigh at right
    with Dissolve(.5)

    h "..."

    show gil worried at center with MoveTransition(1)
    g "Mom still hasn't returned?"

    show mer sigh at left
    m "No. I wonder what could be taking her so long."
    "Everyone" "!"

    show mer curious at right
    show gil normal at center
    show mom normal at left
    with MoveTransition(1)

    mom "I'm so sorry everyone. The line was awfully long. Let's go home now."

    scene black
    with Dissolve(.5)

    jump ch2



