﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

init python:
    config.empty_window = nvl_show_core
    config.window_hide_transition = dissolve
    config.window_show_transition = dissolve

# Declare characters used by this game.
define n = Character(None, kind=nvl, cps=10)
define m = Character('Merlyn', color="#c8ffc8")
define h = Character('Holly', color="#81F7D8")
define g = Character('Gillard', color="#F781F3")
define a = Character('Arthur', color="#2E64FE")
define mom = Character('Mom', color ="#FE9A2E")

#All backgrounds
image bg arch day night = "arch day night.jpg"
image bg arch day = "arch day.jpg"
image bg crossing street night = "crossing street night.jpg"
image bg crossing street day = "crossing street day.jpg"
image bg empty neigh night = "empty neigh night.jpg"
image bg houses bird night = "houses bird view night.jpg"
image bg park2 night = "park2 night.jpg"
image bg playground night = "playground night.jpg"
image bg old house = "mer old house.jpg"
image bg old house gray = im.Grayscale("mer old house.jpg")
image bg old house night = "mer old house night.jpg"
image bg stores day = "stores day.jpg"
image bg mer room day window open = "mer room day window open.jpg"
image bg mer room night window close = "mer room night window close.jpg"
image bg mer old house foyer night lights = "mer old house foyer night lights.jpg"
image bg mer old house foyer day = "mer old house foyer day.jpg"
image bg cafe day = "cafe day.jpg"

#All Merlyn sprites
image mer void = "be2_b11_1_houshin1star.png"
image mer void2 hand = "be2_b12_1_houshin2star.png"
image mer worry = "be2_b11_komaru2.png"
image mer fierce = "be2_b11_majime4.png"
image mer sigh = "be2_b11_nayamu1.png"
image mer aww = "be2_b11_1_nayamu3.png"
image mer sigh hand = "be2_b12_nayamu3.png"
image mer smirk = "be2_b11_def2.png"
image mer smirk welcome = "be2_b14_futeki1.png"
image mer irritated = "be2_b11_fuman3.png"
image mer hmm = "be2_b11_fuman1.png"
image mer curious = "be2_b11_fukigen2.png"
image mer shock = "be2_b11_odoroki1.png"
image mer blush hand = "be2_b12_1_hajirai1.png"

#All Holly sprites
image hol ouch = "jes_c11_atya1.png"
image hol happy = "jes_c11_def1.png"
image hol determined = "jes_c11_futeki2.png"
image hol no way = "jes_c11_ikari1.png"
image hol omg = "jes_c11_ikari2.png"
image hol listening = "jes_c11_majime1.png"
image hol surprise = "jes_c11_odoroki1.png"
image hol sigh = "jes_c11_nayamu1.png"

#All Gillard sprites
image gil uhh = "rio_a11_akire1.png"
image gil huh = "rio_a11_akire2.png"
image gil normal = "rio_a11_def1.png"
image gil i see = "rio_a11_def3.png"
image gil angry = "rio_a12_ikari1.png"
image gil worried = "rio_a13_fuman1.png"

#All Arthur sprites
image ar normal = "wil_a11_def1.png"
image ar mmm = "wil_a11_komaru1.png"
image ar sigh = "wil_a11_nayamu1.png"
image ar smirk = "wil_a11_niyari1.png"
image ar hmm = "wil_a21_def1.png"
image ar disbelief = "wil_a21_komaru1.png"

#All Mom sprites
image mom normal = "eva_a11_def1.png"
image mom smile = "eva_a11_hohoemi1.png"
image mom worry = "eva_a11_komaru1.png"
image mom stare = "eva_a11_majime1.png"
image mom laugh = "eva_a11_warai1.png"

#All stats used throughout the game will be defaulted here
$wallet = 10
$happiness = 50
$hunger = 100
$thirst = 100
$energy = 100
$magic_power = 100

#Common variables to be used
$sleep = False
$eat = False
$drink = False
$day_count = 0

# The game starts here.
label start:
    jump prologue

    return
