label ch2:           
    scene bg park2 night
    with Dissolve(1)
    play music "Yusuke_Tsutsumi_-_05_-_The_Beach.mp3" fadeout 1

    window show
    n "--CHAPTER 2--"
    window hide
    nvl clear

    scene bg playground night
    with Dissolve(.5)

    show mer worry at center

    m "...I wonder how Arthur is doing now..."
    
    show mer sigh at center
    m "I haven't seen him since the day that..."

    show mer curious at right with move
    m "!"

    show mom worry at left with dissolve

    mom "Merlyn...!"

    show mom worry at center with MoveTransition(1)
    show mer worry at right
    mom "Why are you out so late? Do you know how scared I was when I found you missing from your bed?"

    show mer aww at right
    m "...I was just getting some fresh air."

    show mom stare at center
    mom "..."
    show mom worry at center
    mom "...It bothers me still, too, sweetie. But please..."
    show mom stare at center
    mom "Let's not go running off, shall we?"

    show mer worry at right
    m "...All right."

    show mom smile at center
    mom "Let's head back home now."

    scene bg old house night
    with Dissolve(1.5)

    play sound "Close Door-SoundBible.com-1027341191.mp3"
    scene bg mer old house foyer night lights
    show mer worry at left
    show mom worry at right
    with Dissolve(1)

    mom "..."
    m "..."
    show mom stare at right
    mom "Tomorrow you have class at 10 AM, no?"

    show mer aww at left
    m "Yeah..."

    show mom normal at right
    mom "Good night then..."

    hide mom normal with dissolve 

    m "..."

    hide mer aww with dissolve

    scene bg mer room night window close
    with dissolve

    jump ch3